# Exercici John Jcasado:

### Perquè serveixen les comandes següents:
**vgscan:** Examina tots els discos suportats en el sistema en busca de volums LVM.

**vgchange  -ay metasploitable:** Modificar els paràmetres dels grups de volums.

**lvs:** Serveix per veure les particions lògiques

**mkdir disco:** Crear un directori anomenat disco.

**mount /dev/metasploitable:** ens serveix per muntar dispositius o particions.

**cd disco:** Ens permet entrar al directori disco.

![imatge](/home/usuari/Projectes/exercici-john/Imatges/2.png)

### unshadow passwd shadow > r00t4john:
![imatge](/home/usuari/Projectes/exercici-john/Imatges/3.png)

### Crack de passwords senzill:
#### john  --single r00t4john
![imatge](/home/usuari/Projectes/exercici-john/Imatges/4.png)

### Crack de passwords incremental:

#### john --incremental:alpha r00t4john
![imatge](/home/usuari/Projectes/exercici-john/Imatges/5.png)

### Crack de passwords amb regles:

#### john --wordlist=/usr/share/wordlist/500-worst-passwors.txt --rules r00t4john

#### john --wordlist/home/kali/Downloads/rockyou.txt --rules r00t4john

![imatge](/home/usuari/Projectes/exercici-john/Imatges/6.png)

### Contrasenyes:

usuaris  | passwords
--|--
  sys|batman
klog  |123456789
msfadmin  |hopeless
postgres  |postgres
user  |user
service  |service
pau  |kakaroto
roger |banshee
oscar  |oscar123    |

![imatge](/home/usuari/Projectes/exercici-john/Imatges/7.png)  

